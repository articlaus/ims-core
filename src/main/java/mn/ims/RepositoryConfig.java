package mn.ims;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

//import org.modelmapper.ModelMapper;
//import org.springframework.context.annotation.Bean;

/**
 * Created by ganbat on 8/24/16.
 */

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        /**
         * Тодорхойлсон entity object-н ID property-г json result-р дамжуулахыг зөвшөөрүүлэх хэсэг
         */

        super.configureRepositoryRestConfiguration(config);
    }
}
