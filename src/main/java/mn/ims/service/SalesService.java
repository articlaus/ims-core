package mn.ims.service;

import lombok.extern.log4j.Log4j;
import mn.ims.dao.AccountDAO;
import mn.ims.dao.BranchDAO;
import mn.ims.dao.SaleDAO;
import mn.ims.dao.SaleReceiptDAO;
import mn.ims.entity.Account;
import mn.ims.entity.Sales;
import mn.ims.entity.SalesReceipt;
import mn.ims.model.DateBetweenModel;
import mn.ims.model.PageRequestModel;
import mn.ims.model.sales.SaleReceiptCreateModel;
import mn.ims.model.sales.SalesCreateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

@Service
@Log4j
public class SalesService {

    private static SimpleDateFormat smt = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    SaleDAO saleDAO;
    @Autowired
    BranchDAO branchDAO;
    @Autowired
    AccountDAO accountDAO;
    @Autowired
    SaleReceiptDAO receiptDAO;
    @Autowired
    InventoryMovementService movementService;

    public ResponseEntity getByBranch(PageRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<Sales> sales = saleDAO.findByBranchId((Long) requestModel.getData(), pageRequest);
            for (Sales sale : sales.getContent()) {
                sale.setBranch(branchDAO.findOne(sale.getBranchId()).getBranchName());
            }
            return ResponseEntity.ok(sales);
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getMonthlySum(DateBetweenModel model) {
        try {
            BigDecimal sum = saleDAO.getSumByCompanyIdBetweenDate(smt.parse(model.getStartDate()), smt.parse(model.getEndDate()), (Long) model.getData());
            return ResponseEntity.ok(sum);
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity createSale(SalesCreateModel createModel) {
        try {
            Account account = accountDAO.findByUsername(AuthService.getUsername());
            Sales sales = new Sales();
            sales.setCompanyId(account.getCompanyId());
            if (account.getBranchId() != null)
                sales.setBranchId(account.getBranchId());
            sales.setNode(createModel.getNote());
            sales.setSaleAmount(createModel.getSaleAmount());
            sales.setSaleDate(createModel.getSaleDate());
            sales.setSaleUserId(account.getId());
            sales.setSaleBaseAmount(createModel.getBaseAmount());
            saleDAO.save(sales);
            for (SaleReceiptCreateModel receiptCreateModel : createModel.getReceipts()) {
                SalesReceipt receipt = new SalesReceipt();
                receipt.setSaleId(sales.getId());
                receipt.setBranchId(account.getBranchId());
                receipt.setCompanyId(account.getCompanyId());
                receipt.setProductId(receiptCreateModel.getProductId());
                receipt.setQuantity(receiptCreateModel.getQuantity());
                receipt.setSalePrice(receiptCreateModel.getSalePrice());
                receiptDAO.save(receipt);
            }
            movementService.processSaleReceipt(createModel.getReceipts(), account.getBranchId(), account.getId(), createModel.getSaleDate());
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

}
