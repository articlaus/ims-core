package mn.ims.service;

import lombok.extern.log4j.Log4j;
import mn.ims.constants.SystemConstants;
import mn.ims.dao.*;
import mn.ims.entity.Account;
import mn.ims.entity.AccountRoles;
import mn.ims.entity.Role;
import mn.ims.helpers.AccountHelper;
import mn.ims.model.account.AccountModel;
import mn.ims.model.account.AccountPasswordChangeModel;
import mn.ims.model.account.AccountStatusChangeModel;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Log4j
public class UserService {

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    AccountDAO accountDao;

    @Autowired
    AccountRolesDAO rolesDAO;
    @Autowired
    BranchDAO branchDAO;

    @Autowired
    CompanyDAO companyDAO;

    public ResponseEntity createUser(AccountModel model) {
        try {
            if (!AuthService.checkRole(SystemConstants.Role.ADMIN)) {
                Account creator = accountDao.findByUsername(AuthService.getUsername());
                model.setCompanyId(creator.getCompanyId());
            }
            Account account = AccountHelper.modelToEntity(model);
            account.setCreatedDate(new Date());
            StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
            account.setPassword(passwordEncryptor.encryptPassword(account.getPassword()));
            accountDao.save(account);
            Role role = roleDAO.getOne(model.getRoleId());
            AccountRoles accountRoles = new AccountRoles();
            accountRoles.setUserId(account.getId());
            accountRoles.setRoles(role.getRole());
            rolesDAO.save(accountRoles);
            return ResponseEntity.ok("success");
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity updateProfile(AccountModel accountModel) {
        try {
            Account account = accountDao.findByUsername(AuthService.getUsername());
            account.setFirstName(accountModel.getFirstName());
            account.setLastName(accountModel.getLastName());
            account.setPhoneNo(accountModel.getPhoneNo());
            account.setEmail(accountModel.getEmail());
            accountDao.save(account);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getProfile() {
        try {
            Account account = accountDao.findByUsername(AuthService.getUsername());
            if (account.getBranchId() != null)
                account.setBranchName(branchDAO.getOne(account.getBranchId()).getBranchName());
            if (account.getCompanyId() != null)
                account.setCompanyName(companyDAO.getOne(account.getCompanyId()).getName());
            return ResponseEntity.ok(account);
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity disableUser(AccountStatusChangeModel model) {
        try {
            Account account = accountDao.getOne(model.getId());
            account.setStatus(model.getStstus());
            accountDao.save(account);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity changePassword(AccountPasswordChangeModel model) {
        try {
            StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
            Account account = accountDao.findByUsername(AuthService.getUsername());
            if (passwordEncryptor.checkPassword(model.getOldPassword(), account.getPassword())) {
                account.setPassword(passwordEncryptor.encryptPassword(model.getNewPassword()));
                accountDao.save(account);
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
