package mn.ims.service;

import lombok.extern.log4j.Log4j;
import mn.ims.dao.AccountDAO;
import mn.ims.dao.BranchDAO;
import mn.ims.dao.CompanyDAO;
import mn.ims.entity.Account;
import mn.ims.entity.Company;
import mn.ims.model.PageRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Log4j
public class CompanyService {

    @Autowired
    CompanyDAO companyDAO;
    @Autowired
    AccountDAO accountDAO;
    @Autowired
    BranchDAO branchDAO;

    public ResponseEntity create(Company company) {
        try {
            companyDAO.save(company);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity update(Company company) {
        try {
            Company entity = companyDAO.getOne(company.getId());
            entity.setAddress(company.getAddress());
            entity.setName(company.getName());
            companyDAO.save(entity);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getEmployees(PageRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<Account> employees = accountDAO.findByCompanyId(Long.valueOf((String) requestModel.getData()), pageRequest);
            for (Account account : employees.getContent()) {
                if (account.getBranchId() != null)
                    account.setBranchName(branchDAO.getOne(account.getBranchId()).getBranchName());
            }
            return ResponseEntity.ok(employees);
        } catch (Exception ex) {
            log.error("ex", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
