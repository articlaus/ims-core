package mn.ims.service;

import lombok.extern.log4j.Log4j;
import mn.ims.dao.BranchDAO;
import mn.ims.dao.InventoryDAO;
import mn.ims.dao.ProductDAO;
import mn.ims.entity.Inventory;
import mn.ims.model.PageRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Log4j
public class InventoryService {

    @Autowired
    InventoryDAO inventoryDAO;
    @Autowired
    ProductDAO productDAO;
    @Autowired
    BranchDAO branchDAO;


    public ResponseEntity getByBranch(PageRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<Inventory> items = inventoryDAO.findByBranchId(Long.valueOf((String) requestModel.getData()), pageRequest);
            for (Inventory inventory : items.getContent()) {
                inventory.setProductName(productDAO.getOne(inventory.getProductId()).getName());
            }
            return ResponseEntity.ok(items);
        } catch (Exception ex) {
            log.error("ex", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getByCompany(PageRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<Inventory> items = inventoryDAO.findByCompanyId(Long.valueOf((String) requestModel.getData()), pageRequest);
            for (Inventory inventory : items.getContent()) {
                inventory.setProductName(productDAO.getOne(inventory.getProductId()).getName());
                inventory.setBranchName(branchDAO.getOne(inventory.getBranchId()).getBranchName());
            }
            return ResponseEntity.ok(items);
        } catch (Exception ex) {
            log.error("ex", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
