package mn.ims.service;


import mn.ims.dao.ProductDAO;
import mn.ims.entity.Product;
import mn.ims.model.PageRequestModel;
import mn.ims.model.product.ProductSearchByCodeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductDAO productDAO;

    public ResponseEntity createProduct(Product product) {
        try {
            productDAO.save(product);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity updateProduct(Product product) {
        try {
            Product entity = productDAO.getOne(product.getId());
            entity.setBasePrice(product.getBasePrice());
            entity.setDescription(product.getDescription());
            entity.setImgUrl(product.getImgUrl());
            entity.setName(product.getName());
            entity.setTypeId(product.getTypeId());
            entity.setCode(product.getCode());
            productDAO.save(product);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getList(PageRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<Product> products = productDAO.findByCompanyId(Long.valueOf((String) requestModel.getData()), pageRequest);
            return ResponseEntity.ok(products);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity searchLike(ProductSearchByCodeModel codeModel) {
        try {
            List<Product> products = productDAO.findByCodeLikeAndCompanyId(codeModel.getCode(), codeModel.getCompanyId());
            return ResponseEntity.ok(products);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
