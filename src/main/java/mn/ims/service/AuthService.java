package mn.ims.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.extern.log4j.Log4j;
import mn.ims.dao.AccountDAO;
import mn.ims.dao.AccountRolesDAO;
import mn.ims.entity.Account;
import mn.ims.entity.AccountRoles;
import mn.ims.model.TokenRequestModel;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Log4j
public class AuthService {

    @Autowired
    AccountDAO accountDAO;

    @Autowired
    AccountRolesDAO accountRoleDAO;

    public ResponseEntity getToken(TokenRequestModel model) {
        try {
            String token;
            StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
            Account account = accountDAO.findByUsername(model.getUsername());
            if (account != null)
                if (passwordEncryptor.checkPassword(model.getPassword(), account.getPassword())) {
                    AccountRoles roles = accountRoleDAO.findByUserId(account.getId());
                    String[] arrayRoles = roles.getRoles().split(",");
                    token = JWT.create().withSubject(account.getUsername()).withArrayClaim("role", arrayRoles).sign(Algorithm.HMAC256(account.getPassword()));
                    return ResponseEntity.ok(token);
                }
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public static String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    public static Boolean checkRole(String role) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
            if (grantedAuthority.getAuthority().equals(role)) {
                return true;
            }
        }
        return false;
    }
}
