package mn.ims.service;

import lombok.extern.log4j.Log4j;
import mn.ims.dao.AccountDAO;
import mn.ims.dao.BranchDAO;
import mn.ims.entity.Account;
import mn.ims.entity.Branch;
import mn.ims.model.PageRequestModel;
import mn.ims.model.branch.BranchChangeHeadRequestModel;
import mn.ims.model.branch.BranchChangeRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Log4j
public class BranchService {

    @Autowired
    AccountDAO accountDAO;

    @Autowired
    BranchDAO branchDAO;

    public ResponseEntity createBranch(Branch branch) {
        try {
            branchDAO.save(branch);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity updateBranch(Branch branch) {
        try {
            Branch entity = branchDAO.findOne(branch.getId());
            entity.setBranchName(branch.getBranchName());
            entity.setBranchAddress(branch.getBranchAddress());
            branchDAO.save(branch);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity setBranchHead(BranchChangeHeadRequestModel requestModel) {
        try {
            Branch branch = branchDAO.findOne(requestModel.getBranchId());
            branch.setUserId(requestModel.getUserId());
            branchDAO.save(branch);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getEmployees(PageRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<Account> employees = accountDAO.findByBranchId(Long.valueOf((String) requestModel.getData()), pageRequest);
            for (Account account : employees.getContent()) {
                if (account.getBranchId() != null)
                    account.setBranchName(branchDAO.getOne(account.getBranchId()).getBranchName());
            }
            return ResponseEntity.ok(employees);
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity changeBranch(BranchChangeRequestModel requestModel) {
        try {
            Account account;
            for (Long aLong : requestModel.getIds()) {
                account = accountDAO.getOne(aLong);
                account.setBranchId(requestModel.getBranchId());
                accountDAO.save(account);
            }
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
