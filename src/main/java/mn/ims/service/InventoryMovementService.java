package mn.ims.service;

import lombok.extern.log4j.Log4j;
import mn.ims.constants.SystemConstants;
import mn.ims.dao.*;
import mn.ims.entity.*;
import mn.ims.helpers.MovementHelper;
import mn.ims.model.inventory.MovementListRequestModel;
import mn.ims.model.inventory.MovementModel;
import mn.ims.model.inventory.MovementStatusChangeModel;
import mn.ims.model.inventory.ProductMovementModel;
import mn.ims.model.sales.SaleReceiptCreateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Log4j
public class InventoryMovementService {


    @Autowired
    InventoryMovementDAO inventoryMovementDAO;
    @Autowired
    InventoryMovementDetailDAO detailDAO;
    @Autowired
    AccountDAO accountDAO;
    @Autowired
    InventoryDAO inventoryDAO;
    @Autowired
    BranchDAO branchDAO;
    @Autowired
    MovementStatusHistoryDAO historyDAO;
    @Autowired
    ProductDAO productDAO;

    /**
     * Хөдөлгөөн эхлүүлэх функц
     * төрөл үргэлж бэлэн байдалд үүснэ
     * төрлийг гараар солино
     *
     * @param model
     * @return
     */
    public ResponseEntity createMovement(MovementModel model) {
        try {
            InventoryMovement inventoryMovement = new InventoryMovement();
            inventoryMovement.setStatus(SystemConstants.Movement.PREPARED);
            Account user = accountDAO.findByUsername(AuthService.getUsername());
            inventoryMovement.setCreatedUserId(user.getId());
            inventoryMovement.setCreatedDate(new Date());
            inventoryMovement.setFromBranchId(model.getFromBranch());
            inventoryMovement.setToBranchId(model.getToBranch());
            inventoryMovementDAO.save(inventoryMovement);
            for (ProductMovementModel productMovementModel : model.getProductList()) {
                InventoryMovementDetail movementDetail = new InventoryMovementDetail();
                movementDetail.setBarcode(productMovementModel.getBarcode());
                movementDetail.setProductId(productMovementModel.getProductId());
                movementDetail.setInventoryMovementId(inventoryMovement.getId());
                movementDetail.setQuantity(productMovementModel.getQuantity());
                detailDAO.save(movementDetail);
            }
            inventoryMovement.setStatus(null);
            recordMovementHistory(inventoryMovement, SystemConstants.Movement.PREPARED, user.getId());
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity changeStatus(MovementStatusChangeModel model) {
        try {
            InventoryMovement movement = inventoryMovementDAO.findOne(model.getId());
            recordMovementHistory(movement, model.getStatus(), accountDAO.findByUsername(AuthService.getUsername()).getId());
            movement.setStatus(model.getStatus());
            if (SystemConstants.Movement.RECEIVED.equals(model.getStatus())) {
                processAccept(model.getId());
            }
            inventoryMovementDAO.save(movement);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            log.error("error", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private void recordMovementHistory(InventoryMovement movement, Integer newStatus, Long userId) {
        try {
            MovementStatusHistory history = new MovementStatusHistory();
            history.setChangeDate(new Date());
            history.setMovementId(movement.getId());
            history.setPrevStatus(movement.getStatus());
            history.setNewStatus(newStatus);
            history.setUserId(userId);
            historyDAO.save(history);
        } catch (Exception ex) {
            log.error("error", ex);
        }
    }

    /**
     * Хүлээн авсан салбар ok болход үндсэн тооцоог нэгтгнэ агуулахийн тоог шинэжлэн
     *
     * @param id movement id
     */
    private void processAccept(Long id) {
        try {
            InventoryMovement movement = inventoryMovementDAO.findOne(id);
            List<InventoryMovementDetail> items = detailDAO.findByInventoryMovementId(id);
            for (InventoryMovementDetail item : items) {
                Inventory from = inventoryDAO.findByBranchIdAndProductId(movement.getFromBranchId(), item.getProductId());
                from.setQuantity(from.getQuantity() - item.getQuantity());
                inventoryDAO.save(from);
                Inventory to = inventoryDAO.findByBranchIdAndProductId(movement.getToBranchId(), item.getProductId());
                if (to != null)
                    to.setQuantity(to.getQuantity() + item.getQuantity());
                else {
                    to = new Inventory();
                    to.setQuantity(item.getQuantity());
                    to.setProductId(item.getProductId());
                    to.setBranchId(movement.getToBranchId());
                    to.setCompanyId(branchDAO.getOne(movement.getToBranchId()).getId());
                    inventoryDAO.save(to);
                }
            }
        } catch (Exception ex) {
            log.error("error", ex);
        }
    }

    /**
     * Борлуулалт хийгдхээр хөдөлгөөнөөр бүртгэх үндсэн агуулахаас хасах функц
     *
     * @param receipts зарагдсан бараа
     * @param branchId салбар
     * @param userId   зарсан ажилтан
     * @param saleDate борлуулалтийн огноо
     * @throws Exception гарсан алдаа
     */
    public void processSaleReceipt(List<SaleReceiptCreateModel> receipts, Long branchId, Long userId, Date saleDate) throws Exception {
        InventoryMovement movement = new InventoryMovement();
        movement.setStatus(SystemConstants.Movement.SOLD);
        movement.setFromBranchId(branchId);
        movement.setCreatedUserId(userId);
        movement.setCreatedDate(new Date());
        movement.setMovementDate(saleDate);
        inventoryMovementDAO.save(movement);
        for (SaleReceiptCreateModel receipt : receipts) {
            InventoryMovementDetail detail = new InventoryMovementDetail();
            detail.setQuantity(receipt.getQuantity());
            detail.setInventoryMovementId(movement.getId());
            detail.setProductId(receipt.getProductId());
            detail.setBarcode(productDAO.findByCodeAndCompanyId(productDAO.getOne(receipt.getProductId()).getCode(), accountDAO.getOne(userId).getCompanyId()).getCode());
            detailDAO.save(detail);
        }
        inventoryMovementDAO.flush();
        processAccept(movement.getId());
    }


    public ResponseEntity getListByDateAndCompany(MovementListRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<InventoryMovement> movements = inventoryMovementDAO.findByMovementDateBetweenAndCompanyId(requestModel.getStartDate(), requestModel.getEndDate(), requestModel.getCompanyId(), pageRequest);
            for (InventoryMovement inventoryMovement : movements.getContent()) {
                inventoryMovement.setStatusText(MovementHelper.convertStatus(inventoryMovement.getStatus()));
            }
            return ResponseEntity.ok(movements);
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getListByDateAndFromBranch(MovementListRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<InventoryMovement> movements = inventoryMovementDAO.findByMovementDateBetweenAndFromBranchId(requestModel.getStartDate(), requestModel.getEndDate(), requestModel.getFromBranchId(), pageRequest);
            for (InventoryMovement inventoryMovement : movements.getContent()) {
                inventoryMovement.setStatusText(MovementHelper.convertStatus(inventoryMovement.getStatus()));
            }
            return ResponseEntity.ok(movements);
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getListByDateAndToBranch(MovementListRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<InventoryMovement> movements = inventoryMovementDAO.findByMovementDateBetweenAndToBranchId(requestModel.getStartDate(), requestModel.getEndDate(), requestModel.getToBranchId(), pageRequest);
            for (InventoryMovement inventoryMovement : movements.getContent()) {
                inventoryMovement.setStatusText(MovementHelper.convertStatus(inventoryMovement.getStatus()));
            }
            return ResponseEntity.ok(movements);
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getListByDateAndToBranchAndFrom(MovementListRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<InventoryMovement> movements = inventoryMovementDAO.findByMovementDateBetweenAndFromBranchIdAndToBranchId(requestModel.getStartDate(), requestModel.getEndDate(), requestModel.getFromBranchId(), requestModel.getToBranchId(), pageRequest);
            for (InventoryMovement inventoryMovement : movements.getContent()) {
                inventoryMovement.setStatusText(MovementHelper.convertStatus(inventoryMovement.getStatus()));
            }
            return ResponseEntity.ok(movements);
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getListByFromBranch(MovementListRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<InventoryMovement> movements = inventoryMovementDAO.findByFromBranchId(requestModel.getFromBranchId(), pageRequest);
            for (InventoryMovement inventoryMovement : movements.getContent()) {
                inventoryMovement.setStatusText(MovementHelper.convertStatus(inventoryMovement.getStatus()));
            }
            return ResponseEntity.ok(movements);
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getListByToBranch(MovementListRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<InventoryMovement> movements = inventoryMovementDAO.findByToBranchId(requestModel.getToBranchId(), pageRequest);
            for (InventoryMovement inventoryMovement : movements.getContent()) {
                inventoryMovement.setStatusText(MovementHelper.convertStatus(inventoryMovement.getStatus()));
            }
            return ResponseEntity.ok(movements);
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    public ResponseEntity getListByFromBranchAndStatus(MovementListRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<InventoryMovement> movements = inventoryMovementDAO.findByStatusAndFromBranchId(requestModel.getStatus(), requestModel.getFromBranchId(), pageRequest);
            for (InventoryMovement inventoryMovement : movements.getContent()) {
                inventoryMovement.setStatusText(MovementHelper.convertStatus(inventoryMovement.getStatus()));
            }
            return ResponseEntity.ok(movements);
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getListByToBranchAndStatus(MovementListRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<InventoryMovement> movements = inventoryMovementDAO.findByStatusAndToBranchId(requestModel.getStatus(), requestModel.getToBranchId(), pageRequest);
            for (InventoryMovement inventoryMovement : movements.getContent()) {
                inventoryMovement.setStatusText(MovementHelper.convertStatus(inventoryMovement.getStatus()));
            }
            return ResponseEntity.ok(movements);
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity getBranchFullHistory(MovementListRequestModel requestModel) {
        try {
            PageRequest pageRequest = new PageRequest(requestModel.getPage(), requestModel.getSize(), Sort.Direction.DESC, "id");
            Page<InventoryMovement> movements = inventoryMovementDAO.findByToBranchIdOrFromBranchId(requestModel.getFromBranchId(), pageRequest);
            for (InventoryMovement inventoryMovement : movements.getContent()) {
                inventoryMovement.setStatusText(MovementHelper.convertStatus(inventoryMovement.getStatus()));
            }
            return ResponseEntity.ok(movements);
        } catch (Exception ex) {
            log.error("erro", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
