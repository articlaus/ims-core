package mn.ims.constants;

public interface SystemConstants {

    interface Role {
        String ADMIN = "admin";
        String CEO = "ceo";
        String EMPLOYEE = "emp";
    }

    interface Movement {
        String RECIEVED_TEXT = "Хүлээн авсан";
        String TRANSIT_TEXT = "Тээвэрлэгдэж яваа";
        String PREPARED_TEXT = "Тээвэрлэхэд Бэлэн";
        String REJECT_TEXT = "Хүлээж авхаас татгалзсан";
        String SEND_TEXT = "Илгээсэн";
        String INACTIVE_TEXT = "Идэвхигүй";
        String SOLD_TEXT = "Зарагдсан";
        Integer RECEIVED = 1;
        Integer TRANSIT = 2;
        Integer PREPARED = 3;
        Integer REJECT = 4;
        Integer SEND = 5;
        Integer INACTIVE = 6;
        Integer SOLD = 7;
    }
}
