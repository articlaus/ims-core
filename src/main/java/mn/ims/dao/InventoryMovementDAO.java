package mn.ims.dao;

import mn.ims.entity.InventoryMovement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface InventoryMovementDAO extends JpaRepository<InventoryMovement, Long> {

    Page<InventoryMovement> findByMovementDateBetween(Date startDate, Date endDate, Pageable pageable);

    Page<InventoryMovement> findByMovementDateBetweenAndCompanyId(Date startDate, Date endDate, Long companyId, Pageable pageable);

    Page<InventoryMovement> findByMovementDateBetweenAndFromBranchId(Date startDate, Date endDate, Long fromBranchId, Pageable pageable);

    Page<InventoryMovement> findByMovementDateBetweenAndFromBranchIdAndToBranchId(Date startDate, Date endDate, Long fromBranchId, Long toBranchId, Pageable pageable);

    Page<InventoryMovement> findByMovementDateBetweenAndToBranchId(Date startDate, Date endDate, Long toBranchId, Pageable pageable);

    Page<InventoryMovement> findByStatusAndFromBranchId(Integer status, Long fromBranchId, Pageable pageable);

    Page<InventoryMovement> findByStatusAndToBranchId(Integer status, Long toBranchId, Pageable pageable);

    Page<InventoryMovement> findByFromBranchId(Long fromBranchId, Pageable pageable);

    Page<InventoryMovement> findByToBranchId(Long toBranchId, Pageable pageable);

    Page<InventoryMovement> findByToBranchIdOrFromBranchId(Long branchId, Pageable pageable);

}
