package mn.ims.dao;

import mn.ims.entity.InventoryMovementDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryMovementDetailDAO extends JpaRepository<InventoryMovementDetail, Long> {

    List<InventoryMovementDetail> findByInventoryMovementId(Long inventoryMovementId);
}
