package mn.ims.dao;

import mn.ims.entity.SalesReceipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleReceiptDAO extends JpaRepository<SalesReceipt, Long> {
    List<SalesReceipt> findBySaleId(Long saleId);
}
