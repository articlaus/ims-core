package mn.ims.dao;

import mn.ims.entity.Sales;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;

@Repository
public interface SaleDAO extends JpaRepository<Sales, Long> {

    Page<Sales> findByBranchId(Long branchId, Pageable pageable);

    Page<Sales> findBySaleDateBetweenAndCompanyId(Date startDate, Date endDate, Long companyId, Pageable pageable);

    Page<Sales> findBySaleDateBetweenAndBranchId(Date startDate, Date endDate, Long branchId, Pageable pageable);

    @Query("SELECT sum(s.saleAmount) FROM Sales s WHERE (s.saleDate between ?1 and ?2) and s.companyId=?3 ")
    BigDecimal getSumByCompanyIdBetweenDate(Date startDate, Date endDate, Long companyId);
}
