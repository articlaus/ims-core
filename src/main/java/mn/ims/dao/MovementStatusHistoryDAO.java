package mn.ims.dao;

import mn.ims.entity.MovementStatusHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovementStatusHistoryDAO extends JpaRepository<MovementStatusHistory, Long> {
}
