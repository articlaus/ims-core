package mn.ims.dao;

import mn.ims.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountDAO extends JpaRepository<Account, Long> {

    Account findByUsername(String username);

    Page<Account> findByCompanyId(Long companyId, Pageable pageable);

    Page<Account> findByBranchId(Long companyId, Pageable pageable);
}
