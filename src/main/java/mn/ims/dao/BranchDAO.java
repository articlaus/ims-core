package mn.ims.dao;

import mn.ims.entity.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BranchDAO extends JpaRepository<Branch, Long> {
    List<Branch> findByCompanyId(Long companyId);
}
