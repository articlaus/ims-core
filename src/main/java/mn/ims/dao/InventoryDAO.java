package mn.ims.dao;

import mn.ims.entity.Inventory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryDAO extends JpaRepository<Inventory, Long> {
    Page<Inventory> findByCompanyId(Long companyId, Pageable pageable);

    Page<Inventory> findByBranchId(Long companyId, Pageable pageable);

    Inventory findByBranchIdAndProductId(Long branchId, Long productId);
}
