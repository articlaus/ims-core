package mn.ims.dao;

import mn.ims.entity.AccountRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRolesDAO extends JpaRepository<AccountRoles, Long> {

    AccountRoles findByUserId(Long userId);
}
