package mn.ims.dao;

import mn.ims.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDAO extends JpaRepository<Product, Long> {
    Page<Product> findByCompanyId(Long companyId, Pageable pageable);

    List<Product> findByCodeLikeAndCompanyId(String code, Long companyId);

    Product findByCodeAndCompanyId(String code, Long companyId);
}
