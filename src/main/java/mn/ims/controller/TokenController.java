package mn.ims.controller;

import mn.ims.model.TokenRequestModel;
import mn.ims.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/token")
public class TokenController {

    @Autowired
    AuthService authService;

    @RequestMapping("/getToken")
    public ResponseEntity getToken(@RequestBody TokenRequestModel model) {
        return authService.getToken(model);
    }
}
