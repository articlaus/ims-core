package mn.ims.controller;

import mn.ims.model.DateBetweenModel;
import mn.ims.model.PageRequestModel;
import mn.ims.model.sales.SalesCreateModel;
import mn.ims.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sales")
public class SaleController {

    @Autowired
    SalesService salesService;

    @RequestMapping("/getByBranch")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity getByBranch(@RequestBody PageRequestModel model) {
        return salesService.getByBranch(model);
    }

    @RequestMapping("/createSale")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity create(@RequestBody SalesCreateModel model) {
        return salesService.createSale(model);
    }

    @RequestMapping("/monthlySum")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity getMonthlySum(DateBetweenModel model) {
        return salesService.getMonthlySum(model);
    }
}
