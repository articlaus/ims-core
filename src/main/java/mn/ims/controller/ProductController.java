package mn.ims.controller;

import mn.ims.entity.Product;
import mn.ims.model.product.ProductSearchByCodeModel;
import mn.ims.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @RequestMapping("/create")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity create(@RequestBody Product product) {
        return productService.createProduct(product);
    }

    @RequestMapping("/update")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity update(@RequestBody Product product) {
        return productService.updateProduct(product);
    }

    @RequestMapping("/search")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity search(@RequestBody ProductSearchByCodeModel codeModel) {
        return productService.searchLike(codeModel);
    }
}
