package mn.ims.controller;

import mn.ims.entity.Account;
import mn.ims.model.account.AccountModel;
import mn.ims.model.account.AccountPasswordChangeModel;
import mn.ims.model.account.AccountStatusChangeModel;
import mn.ims.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping("/create")
    @PreAuthorize("hasAnyAuthority('admin','ceo')")
    public ResponseEntity createUser(@RequestBody AccountModel account) {
        return userService.createUser(account);
    }

    @RequestMapping("/changeStatus")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity changeStatus(@RequestBody AccountStatusChangeModel account) {
        return userService.disableUser(account);
    }

    @RequestMapping("/changePassword")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity changePassword(@RequestBody AccountPasswordChangeModel model) {
        return userService.changePassword(model);
    }

    @GetMapping("/getProfile")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity<Account> getProfile() {
        return userService.getProfile();
    }

    @RequestMapping("/updateProfile")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity updateProfile(@RequestBody AccountModel accountModel) {
        return userService.updateProfile(accountModel);
    }
}
