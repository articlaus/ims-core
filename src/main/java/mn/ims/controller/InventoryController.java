package mn.ims.controller;

import mn.ims.model.PageRequestModel;
import mn.ims.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/inventory")
public class InventoryController {

    @Autowired
    InventoryService service;

    @PostMapping("/byBranch")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity getByBranch(@RequestBody PageRequestModel model) {
        return service.getByBranch(model);
    }

    @PostMapping("/byCompany")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity getByCompany(@RequestBody PageRequestModel model) {
        return service.getByCompany(model);
    }
}
