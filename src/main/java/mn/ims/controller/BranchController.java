package mn.ims.controller;

import mn.ims.entity.Branch;
import mn.ims.model.PageRequestModel;
import mn.ims.model.branch.BranchChangeHeadRequestModel;
import mn.ims.model.branch.BranchChangeRequestModel;
import mn.ims.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/branch")
public class BranchController {

    @Autowired
    BranchService branchService;

    @RequestMapping("/create")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity create(@RequestBody Branch branch) {
        return branchService.createBranch(branch);
    }

    @RequestMapping("/changeHead")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity changeHead(@RequestBody BranchChangeHeadRequestModel requestModel) {
        return branchService.setBranchHead(requestModel);
    }

    @RequestMapping("update")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity updateBranch(@RequestBody Branch branch) {
        return branchService.updateBranch(branch);
    }

    @RequestMapping("/employeeList")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity getEmployees(@RequestBody PageRequestModel requestModel) {
        return branchService.getEmployees(requestModel);
    }

    @RequestMapping("/changeBranch")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity changeBranch(@RequestBody BranchChangeRequestModel requestModel) {
        return branchService.changeBranch(requestModel);
    }


}
