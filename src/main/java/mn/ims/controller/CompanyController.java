package mn.ims.controller;

import mn.ims.entity.Company;
import mn.ims.model.PageRequestModel;
import mn.ims.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @RequestMapping("/create")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity create(@RequestBody Company company) {
        return companyService.create(company);
    }

    @RequestMapping("/update")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity update(@RequestBody Company company) {
        return companyService.update(company);
    }

    @RequestMapping("/employeeList")
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    public ResponseEntity getEmployees(@RequestBody PageRequestModel requestModel) {
        return companyService.getEmployees(requestModel);
    }


}
