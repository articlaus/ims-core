package mn.ims.controller;

import mn.ims.model.inventory.MovementListRequestModel;
import mn.ims.model.inventory.MovementModel;
import mn.ims.model.inventory.MovementStatusChangeModel;
import mn.ims.service.InventoryMovementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/movement")
public class ItemMovementController {

    @Autowired
    InventoryMovementService service;


    @RequestMapping("/process")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    @Transactional
    public ResponseEntity process(@RequestBody MovementStatusChangeModel model) {
        return service.changeStatus(model);
    }

    @RequestMapping
    @PreAuthorize("hasAnyAuthority('admin','CEO')")
    @Transactional
    public ResponseEntity createMovement(@RequestBody MovementModel model) {
        return service.createMovement(model);
    }

    @RequestMapping("/listByDateAndCompany")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity getListByDateAndCompany(@RequestBody MovementListRequestModel model) {
        return service.getListByDateAndCompany(model);
    }

    @RequestMapping("/listByDateAndFromBranch")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity getListByDateAndFromBranch(@RequestBody MovementListRequestModel model) {
        return service.getListByDateAndFromBranch(model);
    }

    @RequestMapping("/listByDateAndToBranch")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity getListByDateAndToBranch(@RequestBody MovementListRequestModel model) {
        return service.getListByDateAndToBranch(model);
    }

    @RequestMapping("/listByDateAndToBranchAndFrom")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity getListByDateAndToBranchAndFrom(@RequestBody MovementListRequestModel model) {
        return service.getListByDateAndToBranchAndFrom(model);
    }

    @RequestMapping("/listByFromBranch")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity getListByFromBranch(@RequestBody MovementListRequestModel model) {
        return service.getListByFromBranch(model);
    }

    @RequestMapping("/listByToBranch")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity getListByToBranch(@RequestBody MovementListRequestModel model) {
        return service.getListByToBranch(model);
    }


    @RequestMapping("/branchHistory")
    @PreAuthorize("hasAnyAuthority('admin','CEO','EMP')")
    public ResponseEntity getBranchHistory(@RequestBody MovementListRequestModel model) {
        return service.getBranchFullHistory(model);
    }

}
