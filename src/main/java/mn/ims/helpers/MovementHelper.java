package mn.ims.helpers;

public class MovementHelper {

    public static String convertStatus(Integer status) {
        String text = "";
        switch (status) {
            case 1:
                text = "Хүлээн авсан";
                break;
            case 2:
                text = "Тээвэрлэгдэж яваа";
                break;
            case 3:
                text = "Тээвэрлэхэд Бэлэн";
                break;
            case 4:
                text = "Хүлээж авхаас татгалзсан";
                break;
            case 5:
                text = "Илгээсэн";
                break;
        }
        return text;
    }
}
