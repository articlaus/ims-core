package mn.ims.helpers;

import mn.ims.entity.Account;
import mn.ims.model.account.AccountModel;

public class AccountHelper {

    public static Account modelToEntity(AccountModel model) {
        Account account = new Account();
        account.setUsername(model.getUsername());
        account.setPassword(model.getPassword());
        account.setCompanyId(model.getCompanyId());
        account.setEmail(model.getEmail());
        account.setFirstName(model.getFirstName());
        account.setLastName(model.getLastName());
        account.setPhoneNo(model.getPhoneNo());
        account.setStatus(model.getStatus());
        return account;
    }
}
