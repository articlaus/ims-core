package mn.ims.model.branch;

import lombok.Data;

@Data
public class BranchChangeHeadRequestModel {
    Long branchId;
    Long userId;
}
