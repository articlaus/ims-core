package mn.ims.model.branch;

import lombok.Data;

import java.util.List;

@Data
public class BranchChangeRequestModel {
    List<Long> ids;
    Long branchId;
}
