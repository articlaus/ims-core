package mn.ims.model.sales;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class SalesCreateModel {
    String note;
    BigDecimal saleAmount;
    BigDecimal baseAmount;
    Date saleDate;
    List<SaleReceiptCreateModel> receipts;
}
