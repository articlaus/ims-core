package mn.ims.model.sales;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SaleReceiptCreateModel {
    Long productId;
    Integer quantity;
    BigDecimal salePrice;
}
