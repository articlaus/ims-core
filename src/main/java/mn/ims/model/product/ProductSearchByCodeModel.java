package mn.ims.model.product;

import lombok.Data;

@Data
public class ProductSearchByCodeModel {
    String code;
    Long companyId;
    Long branchId;
}
