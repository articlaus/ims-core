package mn.ims.model;

import lombok.Data;

@Data
public class PageRequestModel {
    Integer page;
    Integer size;
    Object data;
}
