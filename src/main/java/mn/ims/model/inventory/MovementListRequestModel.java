package mn.ims.model.inventory;

import lombok.Data;

import java.util.Date;

@Data
public class MovementListRequestModel {
    Date startDate;
    Date endDate;
    Long companyId;
    Long fromBranchId;
    Long toBranchId;
    Integer page;
    Integer size;
    Integer status;
}
