package mn.ims.model.inventory;

import lombok.Data;

@Data
public class ProductMovementModel {
    Long productId;
    Integer quantity;
    String barcode;
}
