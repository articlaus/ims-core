package mn.ims.model.inventory;

import lombok.Data;

import java.util.List;

@Data
public class MovementModel {
    Long fromBranch;
    Long toBranch;
    List<ProductMovementModel> productList;
}
