package mn.ims.model.inventory;

import lombok.Data;

@Data
public class MovementStatusChangeModel {
    Long id;
    Integer status;
}
