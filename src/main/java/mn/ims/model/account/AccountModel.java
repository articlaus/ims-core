package mn.ims.model.account;

import lombok.Data;

@Data
public class AccountModel {
    String username;
    String password;
    String firstName;
    String lastName;
    Long roleId;
    String phoneNo;
    String email;
    Long companyId;
    Integer status;
}
