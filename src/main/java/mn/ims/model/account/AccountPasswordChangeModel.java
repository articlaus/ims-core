package mn.ims.model.account;

import lombok.Data;

@Data
public class AccountPasswordChangeModel {
    String oldPassword;
    String newPassword;
}
