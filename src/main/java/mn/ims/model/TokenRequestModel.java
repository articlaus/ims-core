package mn.ims.model;

import lombok.Data;

@Data
public class TokenRequestModel {
    String username;
    String password;
}
