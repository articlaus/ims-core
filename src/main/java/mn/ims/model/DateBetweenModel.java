package mn.ims.model;

import lombok.Data;

@Data
public class DateBetweenModel {
    String startDate;
    String endDate;
    Object data;
}
