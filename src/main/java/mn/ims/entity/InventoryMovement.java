package mn.ims.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "INVENTORY_MOVEMENT")
@SequenceGenerator(name = "invenmove_seq", sequenceName = "inverntory_movement_id_seq", allocationSize = 1, initialValue = 1)
public class InventoryMovement {
    @Id
    @GeneratedValue(generator = "invenmove_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    Long fromBranchId;
    Long toBranchId;
    Long companyId;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    Date movementDate;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    Date createdDate;
    Long createdUserId;
    Integer status;
    @Transient
    String employeeName;
    @Transient
    String fromBranch;
    @Transient
    String toBranch;
    @Transient
    String statusText;

}
