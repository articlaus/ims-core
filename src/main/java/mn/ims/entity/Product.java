package mn.ims.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PRODUCTS")
@Data
@SequenceGenerator(name = "products_seq", sequenceName = "PRODUCTS_ID_SEQ", allocationSize = 1, initialValue = 10)
public class Product {
    @Id
    @GeneratedValue(generator = "products_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    String name;
    String imgUrl;
    BigDecimal basePrice;
    Integer typeId;
    String description;
    String code;
    Long companyId;
}
