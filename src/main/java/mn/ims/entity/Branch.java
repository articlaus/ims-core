package mn.ims.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "BRANCHES")
@SequenceGenerator(name = "branch_seq", sequenceName = "branches_id_seq", allocationSize = 1, initialValue = 1)
public class Branch {
    @Id
    @GeneratedValue(generator = "branch_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    String branchName;
    @JsonIgnore
    Long companyId;
    String branchAddress;
    Integer status;
    @JsonIgnore
    Long userId;
    @Transient
    String head;


}
