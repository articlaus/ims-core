package mn.ims.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "USER_ROLES")
@Data
@SequenceGenerator(name = "ur_seq", sequenceName = "user_roles_id_seq", allocationSize = 1, initialValue = 1)
public class AccountRoles {

    @Id
    @GeneratedValue(generator = "ur_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    Long userId;
    String roles;
}
