package mn.ims.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "INVENTORY_MOVEMENT_DETAIL")
@SequenceGenerator(name = "imd_seq", sequenceName = "inventory_movement_detail_id_seq", allocationSize = 1)
public class InventoryMovementDetail {
    @Id
    @GeneratedValue(generator = "imd_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    Long productId;
    Long inventoryMovementId;
    Integer quantity;
    String barcode;
}
