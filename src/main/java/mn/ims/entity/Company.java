package mn.ims.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "COMPANIES")
@SequenceGenerator(name = "company_seq", sequenceName = "companies_id_seq", initialValue = 1, allocationSize = 1)
public class Company {
    @Id
    @GeneratedValue(generator = "company_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    String name;
    Long ownerId;
    String address;
}
