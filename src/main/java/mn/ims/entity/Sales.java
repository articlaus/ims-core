package mn.ims.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "SALES")
@Data
@SequenceGenerator(name = "sales_seq", allocationSize = 1, sequenceName = "SALES_ID_SEQ")
public class Sales {
    @Id
    @GeneratedValue(generator = "sales_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    Long branchId;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    Date saleDate;
    BigDecimal saleAmount;
    BigDecimal saleBaseAmount;
    Long saleUserId;
    Long companyId;
    String node;
    @Transient
    String branch;
}
