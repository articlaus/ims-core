package mn.ims.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "MOVEMENT_STATUS_HISTORY")
@SequenceGenerator(name = "mov_seq", sequenceName = "movement_status_history_id_seq", allocationSize = 1)
public class MovementStatusHistory {
    @Id
    @GeneratedValue(generator = "mov_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    Long movementId;
    Integer prevStatus;
    Integer newStatus;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    Date changeDate;
    Long userId;
}
