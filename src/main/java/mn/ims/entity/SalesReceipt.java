package mn.ims.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "SALE_RECEIPTS")
@SequenceGenerator(name = "sr_seq", sequenceName = "SALE_RECEIPTS_ID_SEQ", allocationSize = 1)
public class SalesReceipt {
    @Id
    @GeneratedValue(generator = "sr_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    Long saleId;
    Long productId;
    Integer quantity;
    BigDecimal salePrice;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    Date saleDate;
    Long companyId;
    Long branchId;
}
