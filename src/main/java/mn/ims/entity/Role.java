package mn.ims.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ROLES")
@SequenceGenerator(name = "role_seq", sequenceName = "roles_id_seq", allocationSize = 1, initialValue = 1)
public class Role {
    @Id
    @GeneratedValue(generator = "role_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    String role;
    String description;
}
