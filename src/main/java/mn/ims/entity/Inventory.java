package mn.ims.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "INVENTORY")
@SequenceGenerator(name = "inven_seq", initialValue = 1, sequenceName = "inventory_id_seq", allocationSize = 1)
public class Inventory {
    @Id
    @GeneratedValue(generator = "inven_seq", strategy = GenerationType.SEQUENCE)
    @JsonIgnore
    Long id;
    Long companyId;
    Long branchId;
    Long productId;
    Integer quantity;
    Integer status;
    @Transient
    String productName;
    @Transient
    String branchName;
}
