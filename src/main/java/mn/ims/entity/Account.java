package mn.ims.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "USERS")
@SequenceGenerator(name = "user_seq", sequenceName = "users_id_seq", allocationSize = 1, initialValue = 1)
public class Account {
    @Id
    @GeneratedValue(generator = "user_seq", strategy = GenerationType.SEQUENCE)
    Long id;
    String username;
    @JsonIgnore
    String password;
    String firstName;
    String lastName;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    Date createdDate;
    String phoneNo;
    String email;
    Long companyId;
    Integer status;
    Long branchId;
    @Transient
    String branchName;
    @Transient
    String companyName;

}
